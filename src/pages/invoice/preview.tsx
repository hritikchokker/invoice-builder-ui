import React, { useState } from 'react'
import Image from 'next/image';
import styles from '../../styles/invoice.module.css';
function Preview() {
    const [productList, setProductList] = useState<Array<any>>([{
        "itemId": "2",
        "description": "ds",
        "weight": "ds",
        "Quantity": "sda",
        "price": "2343"
    }, {
        "itemId": "1",
        "description": "dfdss",
        "weight": "asdds",
        "Quantity": "sfsdda",
        "price": "432"
    }, {
        "itemId": "4352",
        "description": "dhfghgfs",
        "weight": "dfghs",
        "Quantity": "sdfgha",
        "price": "123"
    }]);
    const [showDownloadBtn, setShowDownloadBtn] = useState(true);

    const handleInvoiceDownload = () => {
        setShowDownloadBtn(false);
        setTimeout(() => {
            window.print();
            setShowDownloadBtn(true);
        }, 800)
    };

    return (
        <div className={styles.preview_wrapper}>
            <section className={styles.preview_header}>
                <h1>Commercial Invoice</h1>
                <div>
                    <Image
                        src="/images/logo.png"
                        alt="image logo"
                        width={100}
                        height={100}
                    />
                </div>
            </section>
            <div className={styles.product_metadata}>
                <section className={styles.preview_exporter}>
                    <h2>Exporter/shipper</h2>
                    <p>ABC trading</p>
                    <p>John DOe</p>
                    <p>273 Hill view columbia SC,22023</p>
                    <p>(123)234-2323</p>
                    <p>test@gmail.com</p>
                </section>
                <section className={styles.preview_exporter}>
                    <h2>Importer/shipper</h2>
                    <p>Personal Home</p>
                    <p>Abraham lincon</p>
                    <p>384334 Huntsville AL,2832828</p>
                    <p>(123)234-823842838</p>
                    <p>main@gmail.com</p>
                </section>
                <section className={styles.preview_exporter}>
                    <div className=''>
                        <h4>Invoice Date</h4>
                        <p>March 18,2019</p>
                    </div>
                    <div className=''>
                        <h4>Invoice #</h4>
                        <p>72378478</p>
                    </div>
                    <div className=''>
                        <h4>Country of origin</h4>
                        <p>India</p>
                    </div>
                    <div className=''>
                        <h4>Country Destination</h4>
                        <p>UAE</p>
                    </div>
                </section>
            </div>
            <section className={styles.preview_products}>
                <h1>Items/Products</h1>
                <div id="tableIds">
                    <table suppressHydrationWarning={true}>
                        <tbody>
                            <tr>
                                <th>Item ID</th>
                                <th>Description</th>
                                <th>Weight</th>
                                <th>Quantity</th>
                                <th>Price($)</th>
                                <th>Total($)</th>
                            </tr>
                            {productList.map((el, index) => {
                                return (
                                    // eslint-disable-next-line react/jsx-key
                                    <tr key={el.itemId + index}>
                                        <td >{el.itemId}</td>
                                        <td >{el.description}</td>
                                        <td >{el.weight}</td>
                                        <td >{el.Quantity}</td>
                                        <td >{el.price}</td>
                                        <td >0</td>
                                    </tr>
                                )
                            })}
                        </tbody>

                    </table>
                </div>
            </section>
            {showDownloadBtn && <div className={styles.download_btn}>
                <button onClick={handleInvoiceDownload}>
                    download invoice
                </button>
            </div>}
        </div>
    )
}

export default Preview;