import React from 'react'
import styles from '../../styles/invoice.module.css';



function Address({ type }: any) {
    return (
        <div className={[styles.flex_row, styles.bg_yellow].join(' ')}>
            <h1>{type}</h1>
            <div className={styles.form_field}>
                <h4>Company Name</h4>
                <input type='text' />
            </div>
            <div className={styles.form_field}>
                <h4> Contact Name</h4>
                <div>
                    <h4>first name</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>last Name</h4>
                    <input type='text' />
                </div>
            </div>
            <div className={styles.form_field}>
                <h4>Address</h4>
                <div>
                    <h4>street address</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>street address line 2</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>city</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>state/province</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>postal / zip code</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>phone number</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>email</h4>
                    <input type='text' />
                </div>
                <div>
                    <h4>Country of origin</h4>
                    <input type='text' />
                </div>
            </div>
        </div>
    )
}

export default Address;