import React from 'react'
import { Formik, Form, Field, ErrorMessage, FieldArray } from 'formik';
import * as Yup from 'yup';
import styles from '../../styles/invoice.module.css';
const productSchema = Yup.object().shape({
    date_of_invoice: Yup.string()
        .required(),
    invoice_number: Yup.string()
        .min(3, 'Too Short')
        .max(50, 'Too BIg'),
    exporter_info: Yup.object().shape({
        companyName: Yup.string()
            .min(3, 'Too Short')
            .max(50, 'Too BIg')
            .required('companyName is required'),
        firstName: Yup.string()
            .min(3, 'Too Short')
            .max(50, 'Too BIg')
            .required('firstName is required'),
        lastName: Yup.string()
            .min(3, 'Too Short')
            .max(50, 'Too BIg')
            .required('lastName is required'),
        address: Yup.object().shape({
            street: Yup.string().required('Street is required'),
            street_line1: Yup.string().required('Street is required'),
            city: Yup.string().required('City is required'),
            state: Yup.string().required('State is required'),
            zip: Yup.string().required('Zip code is required')
        }),
        phone_no: Yup.number()
            .min(8, 'You must be at least 18 years old')
            .required('Age is required'),
        email: Yup.string()
            .min(4, 'Min length')
            .max(80, 'Max Length')
            .required('Email is required')
    }),
    importer_info: Yup.object().shape({
        companyName: Yup.string()
            .min(3, 'Too Short')
            .max(50, 'Too BIg')
            .required('companyName is required'),
        firstName: Yup.string()
            .min(3, 'Too Short')
            .max(50, 'Too BIg')
            .required('firstName is required'),
        lastName: Yup.string()
            .min(3, 'Too Short')
            .max(50, 'Too BIg')
            .required('lastName is required'),
        address: Yup.object().shape({
            street: Yup.string().required('Street is required'),
            street_line1: Yup.string().required('Street is required'),
            city: Yup.string().required('City is required'),
            state: Yup.string().required('State is required'),
            zip: Yup.string().required('Zip code is required')
        }),
        phone_no: Yup.number()
            .min(8, 'You must be at least 18 years old')
            .required('Age is required'),
        email: Yup.string()
            .min(4, 'Min length')
            .max(80, 'Max Length')
            .required('Email is required')
    }),
    product_data: Yup.object().shape({
        purpose_of_export: Yup.string()
            .required(),
        product_data: Yup.object().shape({
            product_list: Yup.array().of(Yup.object().shape({
                itemId: Yup.string()
                    .min(3, 'Min reached')
                    .max(70, 'Maximum reqched')
                    .required('Item id is required'),
                description: Yup.string()
                    .min(3, 'Min reached')
                    .max(70, 'Maximum reqched')
                    .required('description is required'),
                weight: Yup.string()
                    .min(3, 'Min reached')
                    .max(70, 'Maximum reqched')
                    .required('weight is required'),
                Quantity: Yup.number()
                    .min(3, 'Min reached')
                    .max(70, 'Maximum reqched')
                    .required('quantity is required'),
                price: Yup.number()
                    .min(3, 'Min reached')
                    .max(70, 'Maximum reqched')
                    .required('price is required')
            })),
            sub_total: Yup.number()
                .min(4, 'min ')
                .max(40, 'max')
                .required('sub total is required'),
            shipping_fees: Yup.number()
                .min(4, 'min ')
                .max(40, 'max')
                .required('shipping_fees is required'),
            sales_fees: Yup.number()
                .min(4, 'min ')
                .max(40, 'max')
                .required('sales_fees is required'),
            total_amount: Yup.number()
                .min(4, 'min ')
                .max(40, 'max')
                .required('total_amount is required'),
        })
    })
});

const PurposeConsentForm = () => {
    return (
        <div>
            <h2>Purpose Of Export</h2>
            <div>
                <h3>Sale</h3>
            </div>
            <div>
                <h3>Repair</h3>
            </div>
            <div>
                <h3>Return of order</h3>
            </div>
            <div>
                <h3>Personal gift</h3>
            </div>
            <div>
                <h3>other</h3>
            </div>
        </div>
    )
};

const initialValues = {
    "date_of_invoice": "2023-04-13",
    "invoice_number": "137127",
    "exporter_info": {
        "companyName": "abc",
        "firstName": "test",
        "lastName": "user",
        "address": {
            "street": "ausndj",
            "street_line1": "ogfo",
            "city": "delhi",
            "state": "new delhi",
            "zip": "110092"
        },
        "phone_no": "",
        "email": "test@gmail.com",
        "phone": "7823721783"
    },
    "importer_info": {
        "companyName": "xyz",
        "firstName": "imp",
        "lastName": "person",
        "address": {
            "street": "ookdf",
            "street_line1": "mlvmc",
            "city": "delhi",
            "state": "new delhi",
            "zip": "110029"
        },
        "phone_no": "",
        "email": "main@gmail.com",
        "phone": "2178737821"
    },
    "product_data": {
        "purpose_of_export": "",
        "product_data_item": {
            "product_list": [
                {
                    "itemId": "2",
                    "description": "ds",
                    "weight": "ds",
                    "Quantity": "sda",
                    "price": "as"
                }
            ],
            "sub_total": "",
            "shipping_fees": "",
            "sales_fees": "",
            "total_amount": ""
        }
    },
    "items": [
        {
            "itemId": "2",
            "description": "shdhs ",
            "weight": "hsadh",
            "quantity": "shdh",
            "price": 22
        }
    ]
}

function Product() {
    return (
        <div className="">
            <h1>Product</h1>
            {/*  */}
            <Formik
                initialValues={initialValues}
                validationSchema={productSchema}
                onSubmit={(values) => {
                    console.log(values);
                }}
            >
                {({ values, setFieldValue, handleSubmit }) => (
                    <Form onSubmit={handleSubmit}>
                        <div className={styles.flex_row}>
                            <div className={styles.date_details}>
                                <div className={styles.form_field}>
                                    <label htmlFor="date_of_invoice">Date of Invoice</label>
                                    <Field type="date" name="date_of_invoice" />
                                    <ErrorMessage name="date_of_invoice" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="invoice_number">Invoice Number</label>
                                    <Field type="text" name="invoice_number" />
                                    <ErrorMessage name="invoice_number" />
                                </div>
                            </div>

                            <div className={styles.exporter_details}>
                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.companyName">Exporter Company Name</label>
                                    <Field type="text" name="exporter_info.companyName" />
                                    <ErrorMessage name="exporter_info.companyName" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.firstName">Exporter First Name</label>
                                    <Field type="text" name="exporter_info.firstName" />
                                    <ErrorMessage name="exporter_info.firstName" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.lastName">Exporter Last Name</label>
                                    <Field type="text" name="exporter_info.lastName" />
                                    <ErrorMessage name="exporter_info.lastName" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.address.street">Exporter Street</label>
                                    <Field type="text" name="exporter_info.address.street" />
                                    <ErrorMessage name="exporter_info.address.street" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.address.street_line1">Exporter Street Line 1</label>
                                    <Field type="text" name="exporter_info.address.street_line1" />
                                    <ErrorMessage name="exporter_info.address.street_line1" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.address.city">Exporter City</label>
                                    <Field type="text" name="exporter_info.address.city" />
                                    <ErrorMessage name="exporter_info.address.city" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.address.state">Exporter State</label>
                                    <Field type="text" name="exporter_info.address.state" />
                                    <ErrorMessage name="exporter_info.address.state" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.address.zip">Exporter Zip</label>
                                    <Field type="text" name="exporter_info.address.zip" />
                                    <ErrorMessage name="exporter_info.address.zip" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.email">Exporter Email</label>
                                    <Field type="email" name="exporter_info.email" />
                                    <ErrorMessage name="exporter_info.email" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="exporter_info.phone">Exporter Phone</label>
                                    <Field type="tel" name="exporter_info.phone" />
                                    <ErrorMessage name="exporter_info.phone" />
                                </div>
                            </div>

                            <div className={styles.importer_details}>
                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.companyName">Importer Company Name</label>
                                    <Field type="text" name="importer_info.companyName" />
                                    <ErrorMessage name="importer_info.companyName" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.firstName">Importer First Name</label>
                                    <Field type="text" name="importer_info.firstName" />
                                    <ErrorMessage name="importer_info.firstName" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.lastName">Importer Last Name</label>
                                    <Field type="text" name="importer_info.lastName" />
                                    <ErrorMessage name="importer_info.lastName" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.address.street">Importer Street</label>
                                    <Field type="text" name="importer_info.address.street" />
                                    <ErrorMessage name="importer_info.address.street" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.address.street_line1">Importer Street Line 1</label>
                                    <Field type="text" name="importer_info.address.street_line1" />
                                    <ErrorMessage name="importer_info.address.street_line1" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.address.city">Importer City</label>
                                    <Field type="text" name="importer_info.address.city" />
                                    <ErrorMessage name="importer_info.address.city" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.address.state">Importer State</label>
                                    <Field type="text" name="importer_info.address.state" />
                                    <ErrorMessage name="importer_info.address.state" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.address.zip">Importer Zip</label>
                                    <Field type="text" name="importer_info.address.zip" />
                                    <ErrorMessage name="importer_info.address.zip" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.email">Importer Email</label>
                                    <Field type="email" name="importer_info.email" />
                                    <ErrorMessage name="importer_info.email" />
                                </div>

                                <div className={styles.form_field}>
                                    <label htmlFor="importer_info.phone">Importer Phone</label>
                                    <Field type="tel" name="importer_info.phone" />
                                    <ErrorMessage name="importer_info.phone" />
                                </div>
                            </div>

                            <div className={styles.product_list}>
                                <label>Items</label>
                                <FieldArray name="items">
                                    {(arrayHelpers) => (
                                        <div>
                                            {values.product_data.product_data_item.product_list.map((item: any, index: number) => (
                                                <div key={index}>
                                                    <div>
                                                        <label htmlFor={`items.${index}.itemId`}>Item ID</label>
                                                        <Field
                                                            name={`items.${index}.itemId`}
                                                            placeholder="Enter itemId"
                                                            type="text"
                                                        />
                                                        <ErrorMessage name={`items.${index}.itemId`} />
                                                    </div>
                                                    <div>
                                                        <label htmlFor={`items.${index}.description`}>Description</label>
                                                        <Field
                                                            name={`items.${index}.description`}
                                                            placeholder="Enter description"
                                                            type="text"
                                                        />
                                                        <ErrorMessage name={`items.${index}.description`} />
                                                    </div>
                                                    <div>
                                                        <label htmlFor={`items.${index}.weight`}>weight</label>
                                                        <Field
                                                            name={`items.${index}.weight`}
                                                            placeholder="Enter weight"
                                                            type="text"
                                                        />
                                                        <ErrorMessage name={`items.${index}.weight`} />
                                                    </div>
                                                    <div>
                                                        <label htmlFor={`items.${index}.quantity`}>quantity</label>
                                                        <Field
                                                            name={`items.${index}.quantity`}
                                                            placeholder="Enter quantity"
                                                            type="text"
                                                        />
                                                        <ErrorMessage name={`items.${index}.quantity`} />
                                                    </div>
                                                    <div>
                                                        <label htmlFor={`items.${index}.price`}>price</label>
                                                        <Field
                                                            name={`items.${index}.price`}
                                                            placeholder="Enter price"
                                                            type="number"
                                                            step="0.01"
                                                        />
                                                        <ErrorMessage name={`items.${index}.price`} />
                                                    </div>
                                                    <div>
                                                        <button type="button" onClick={() => {
                                                            // arrayHelpers.remove(index)
                                                            const { form: { values: { product_data: { product_data_item: { product_list } } } } } = arrayHelpers;
                                                            product_list.splice(index, 1);
                                                            setFieldValue('values.product_data.product_data_item.product_list', product_list);
                                                        }
                                                        }>
                                                            Remove
                                                        </button>
                                                    </div>
                                                </div>
                                            ))}
                                            <div>
                                                <button type="button" onClick={() => {
                                                    console.log(arrayHelpers, 'array helper');
                                                    const { form: { values: { product_data: { product_data_item: { product_list } } } } } = arrayHelpers;
                                                    product_list.push({
                                                        itemId: '0',
                                                        description: 'sk',
                                                        weight: 'skd',
                                                        Quantity: 'sakd',
                                                        price: 'sdj'
                                                    });
                                                    setFieldValue('values.product_data.product_data_item.product_list', product_list);
                                                }}>
                                                    Add Item
                                                </button>
                                            </div>
                                        </div>
                                    )}
                                </FieldArray>
                            </div>
                        </div>
                        <div className={styles.action_btn}>
                            <button onClick={()=>{
                                console.log(values,'valusdsds');
                            }} type='submit'>submit the form</button>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    );
}

export default Product